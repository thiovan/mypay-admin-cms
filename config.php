<?php
  date_default_timezone_set("Asia/Jakarta");
//   production
  define('BASE_URL', 'http://cms.my-pay.id/');
  define('WEB_URL', 'https://my-pay.id/api');
  define('WEB_URL_NOAPI', 'https://my-pay.id/');

//   development
  // define('BASE_URL', 'http://cms.my-pay.id/');
  // define('WEB_URL', 'https://my-pay.id/development/api');
  // define('WEB_URL_NOAPI', 'https://my-pay.id/development/');

//   local
  // define('BASE_URL', 'http://localhost/admin/');
  // define('WEB_URL', 'http://localhost/mypay-web/public/api');
  // define('WEB_URL_NOAPI', 'http://localhost/mypay-web/public/');

	while (list($key,$val)=each($_GET)){
		${'get_'.$key}=$val;
	}
	// post_formatter
	while (list($key,$val)=each($_POST)){
		${'post_'.$key}=$val;
	}
