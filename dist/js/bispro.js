$(document).ready(function () {
    toggleSidebar()
    sidebarSlimScroll()
    sidebarActiveClass()
})

function toggleSidebar() {
    $('#dash-sidebar-toggler').click(function () {
        $('aside').toggleClass('dash-sidebar-visible')
    })
}

function sidebarSlimScroll() {
    $('.dash-slimscroll').slimScroll({
        height: 'auto',
        alwaysVisible: true,
        wheelStep: 5,
    })
}

function sidebarActiveClass() {
    var current = location.pathname;

    if (current != '/mypay/') {
        $('ul.dash-sidebar-list > li:not(:first-child)').each(function () {
            var $this = $(this);
            var link = $this.children('a').attr('href') + '/'

            if (link.indexOf(current) !== -1) {
                $this.addClass('dash-active')
            }
        })
        $('ul.dash-sidebar-submenu > li a').each(function () {
            var $this = $(this)
            var link = $(this).attr('href') + '/'

            if (link.indexOf(current) !== -1) {
                $this.addClass('dash-submenu-active')
                $this.parents('li.dash-sidebar-hasmenu').addClass('dash-active')
            }
        })
    }
    $('.dash-sidebar-hasmenu').click(function () {
        $(this).toggleClass('dash-active')
    })
}
