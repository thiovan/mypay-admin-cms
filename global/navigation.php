<nav class="navbar fixed-top navbar-expand-lg navbar-light dash-navbar bg-white">
    <a class="navbar-brand dash-navbar-brand py-2" href="<?php echo BASE_URL; ?>">
      <img src="<?php echo BASE_URL ?>dist/img/logo.png" alt="my-pay" class="Center" width="30%">
      <small style="display: block; font-size: 0.5rem; letter-spacing: 0.05rem; color:#000 ">Dashboard - Admin</small>
    </a>
    <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarSupportedContent" data-toggle="collapse" type="button">
        <span class="navbar-toggler-icon">
        </span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo BASE_URL; ?>content">Content</a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-3" href="<?php echo BASE_URL; ?>seo">SEO</a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-3" href="<?php echo BASE_URL; ?>article">Article</a>
        </li>
        <li class="nav-item">
          <a class="nav-link ml-3" href="<?php echo BASE_URL; ?>promo">Promo</a>
        </li>
       
      </ul>
      <ul class="navbar-nav ml-auto mr-3">
        <li class="nav-item dropdown">
            <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="navbarDropdown" role="button">
              User
            </a>
            <div aria-labelledby="navbarDropdown" class="dash-dropdown-menu dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="javascript:void(0)" id="logout">
                  Logout
                </a>
                <span class="badge badge-secondary span-center mt-1" id="loadingLogout"><i class="fas fa-spinner ld ld-cycle"></i> Loading</span>
            </div>
        </li>
      </ul>
    </div>

</nav>
