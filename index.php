<!DOCTYPE html>
<html lang="en">

<?php
date_default_timezone_set("Asia/Jakarta");
require_once "config.php";
if (empty($_GET['page'])) $get_page = "";
if (empty($_GET['sub'])) $get_sub = "";
?>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>My-Pay | Admin</title>
  <link rel="icon" href="<?php echo BASE_URL ?>dist/img/favicon.png" sizes="16x16" type="image/png">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>dist/css/app.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>dist/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>dist/css/quill.snow.css">
  <link rel="stylesheet" href="<?php echo BASE_URL ?>dist/css/all.css">
  <link rel="stylesheet" href="<?php echo BASE_URL; ?>dist/css/loading.css">
  <script src="<?php echo BASE_URL; ?>dist/js/app.js"></script>
  <script>
    jQuery = $
  </script>
  <script src="<?php echo BASE_URL ?>dist/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo BASE_URL ?>dist/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo BASE_URL ?>dist/js/Chart.js"></script>
  <script src="<?php echo BASE_URL ?>dist/js/chart.min.js"></script>
  <script src="<?php echo BASE_URL ?>dist/js/quill.js"></script>
  <script type="text/javascript">
    var base_url="<?php echo BASE_URL ?>";
    var mp_url="<?php echo WEB_URL ?>";
    var mp_url_noapi="<?php echo WEB_URL_NOAPI ?>";

    var menuCharity = 1;
  </script>
  <style media="screen">
    .row-dashboard {
      margin-left: 50px;
      margin-right: 50px;
      margin-top: 20px;
    }

    hr {
      border: 0;
      clear: both;
      display: block;
      width: 96%;
      background-color: #ced4da;
      height: 1px;
    }

    .img-avatar {
      object-fit: cover;
      width: 250px;
      height: 250px;
      box-shadow: 0 3px 5px rgba(0, 0, 0, 0.10);
      margin-left: auto;
      margin-right: auto;
      display: block;
    }

    .span-center {
      display: table;
      margin: 0 auto;
    }

    table.dataTable tbody td {
      vertical-align: middle;
    }
  </style>
</head>

<body style="display:none" id="body">
  <div>
    <!-- Navigation -->
    <?php include 'global/navigation.php'; ?>
    <!-- End of navigation -->
    <!-- Body wrapper -->
    <div class="dash-wrapper">
    <!-- Content -->
    <section class="dash-content py-3 container-fluid">
      <?php
      switch ($get_page) {
        case "content":
          include "views/web-content.php";
          break;
        case "seo":
          include "views/web-seo.php";
          break;
        case "article":
          include "views/web-article.php";
          break;
        case "promo":
          include "views/web-promo.php";
          break;
        default:
          include 'views/web-content.php';
          break;
      }
      ?>
    </section>
    <!-- End of content -->
    </div>
    <!-- End of body wrapper -->
  </div>

  <script src="<?php echo BASE_URL; ?>dist/js/bispro.js"></script>
  <script>
    $(document).ready(function() {
      $("#navbarDropdown").text(localStorage.name)
      document.getElementById("body").style.display = "block";
      $("#loadingPhone").hide()
      $("#loadingLogout").hide()

      var url = document.location.href.split("/")
      $('.nav-item').each(function() {
        var href = $(this).find("a").attr("href").split("/")
        if (url[url.length - 1] == href[href.length - 1]) $(this).addClass('active')
      })
      $('.dropdown-item').each(function() {
        var href = $(this).attr("href").split("/")
        if ($(this).attr("href").indexOf("/") >= 0) {
          if (url[url.length - 1] == href[href.length - 1]) $(this).addClass('active')
        }
      })

      if (!localStorage.token || !localStorage.level) {
        window.location.replace(base_url + "login");
      } else {
        document.getElementById("body").style.display = "block";
      }

      // topup
      $('#nav-phone-topup').keypress(function(e) {
        if (e.which == 13) {
          topup()
        }
      });
    })

    $(document).on("click", "#logout", function() {
      $("#logout").hide()
      $("#loadingLogout").show()
      localStorage.clear()
      window.location.href = base_url + "login";
    })

    $(document).on("click", "#nav-btn-topup", function(e) {
      topup()
    })

    $(document).on("click", "#btnTopUp", function(e) {
      $("#btnTopUp").text("Loading...")
      $("#btnTopUp").prop("disabled", true)
      $("#btnTopUpClose").prop("disabled", true)

      var fd = new FormData()
      fd.append("receiver", $("#topup-phone").text())
      fd.append("amount", $("#topup-amount").val())
      fd.append("pin", $("#topup-pin").val())

      $.ajax({
        type: "POST",
        url: mp_url + "/admin/topup",
        crossDomain: true,
        processData: false,
        contentType: false,
        beforeSend: function(request) {
          request.setRequestHeader("Accept", "application/json");
          request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          request.setRequestHeader("Authorization", localStorage.token);
        },
        data: fd,
        dataType: "json",
        success: function(res) {
          console.log(res)
          $("#topup-alert").text(res.data.message)
          $("#topup-alert").show()
          setTimeout(function() {
            location.reload()
          }, 2000)
        },
        error: function(request, status, error) {
          var response = JSON.parse(request.responseText)
          console.log(response)
          alert(response.message)

          $("#btnTopUp").html("Save Changes")
          $("#btnTopUp").prop("disabled", false)
          $("#btnTopUpClose").prop("disabled", false)
        }
      });
    })

    function rp(x) {
      return "Rp " + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function dateDiff(x = 0, y) {
      if (x == 0) {
        var today = new Date();
        x = today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate()
      }
      var date1 = new Date(x);
      var date2 = new Date(y);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      return diffDays;
    }

    function dateIDN(date) {
      var datetime = date.split(" ")
      var date = datetime[0].split("-")
      var month = ""
      switch (date[1]) {
        case "01":
          month = "Januari";
          break;
        case "02":
          month = "Februari";
          break;
        case "03":
          month = "Maret";
          break;
        case "04":
          month = "April";
          break;
        case "05":
          month = "Mei";
          break;
        case "06":
          month = "Juni";
          break;
        case "07":
          month = "Juli";
          break;
        case "08":
          month = "Agustus";
          break;
        case "09":
          month = "September";
          break;
        case "10":
          month = "Oktober";
          break;
        case "11":
          month = "November";
          break;
        case "12":
          month = "Desember";
          break;
        default:
          break;
      }
      return date[2] + " " + month + " " + date[0] + " " + datetime[1]
    }

    function status(status) {
      switch (status) {
        case 1:
          return "Success";
          break;
        case 2:
          return "Hold";
          break;
        case 3:
          return "Cancel";
          break;
        case 4:
          return "Refund";
          break;
        default:
          return "Unknown";
          break;
      }
    }

    function pad(num, size) {
      var s = num + "";
      while (s.length < size) s = "0" + s;
      return s;
    }

    function trxType(trxNumber) {
      var trx = trxNumber.split("-")
      switch (trx[0]) {
        case "MPW":
          return "Withdraw";
          break;
        case "MPT":
          return "Transfer";
          break;
        case "MPC":
          return "Charity";
          break;
        case "MTC":
          return "Topup Cash";
          break;
        case "MTL":
          return "Topup LinkAja";
          break;
        case "MTM":
          return "Topup MidTrans";
          break;
        case "MSP":
          return "MyStore";
          break;
        case "TSP":
          return "T-Store";
          break;
        case "MPE":
          return "MyPay Event";
          break;
        case "MBL":
          return "MyPay Bills";
          break;
        default:
          return "Unknown";
          break;
      }
    }

    function topup() {
      $("#loadingPhone").show()
      $("#topup-alert").hide()
      var fd = new FormData()
      fd.append("phone", $("#nav-phone-topup").val())

      $.ajax({
        type: "POST",
        url: mp_url + "/accounts/find",
        crossDomain: true,
        processData: false,
        contentType: false,
        beforeSend: function(request) {
          request.setRequestHeader("Accept", "application/json");
          request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          request.setRequestHeader("Authorization", localStorage.token);
        },
        data: fd,
        dataType: "json",
        success: function(res) {
          $("#loadingPhone").hide()
          $("#topup-phone").text(res.data.phone)
          $("#topup-name").text(res.data.name)
          $("#modalTopUp").modal("show")
        },
        error: function(request, status, error) {
          $("#loadingPhone").hide()
          var response = JSON.parse(request.responseText)
          console.log(response)
          alert(response.message)
        }
      });
    }
  </script>
</body>

</html>