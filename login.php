<?php
  require_once "config.php";
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<!-- <script>jQuery = $</script> -->
<!------ Include the above in your HEAD tag ---------->

<head>
  <style media="screen">
  body {
    margin: 0;
    padding: 0;
    background-color: #003C7F;
    height: 100vh;
  }
  #login .container #login-row #login-column #login-box {
    margin-top: 50px;
    max-width: 600px;
    border: 1px solid #9C9C9C;
    background-color: #EAEAEA;
  }
  #login .container #login-row #login-column #login-box #login-form {
    padding: 20px;
  }
  #login .container #login-row #login-column #login-box #login-form #register-link {
    margin-top: -85px;
  }
  .center {
    display: block;
    margin-left: auto;
    margin-right: auto;
    width: 50%;
    margin-top: 20px;
  }

  </style>
  <title>My-Pay | Login</title>
</head>

<body style="display:none" id="body">
  <div id="login">
    <!-- <h3 class="text-center text-white pt-5">My-Pay User Login</h3> -->
    <div class="container">
      <div id="login-row" class="row justify-content-center align-items-center">
        <div id="login-column" class="col-md-6">
          <div id="login-box" class="col-md-12">
            <img src="<?php echo BASE_URL ?>dist/img/logo.png" alt="my-pay" class="Center">
              <form id="login-form" class="form" autocomplete="off" >
                  <div class="form-group">
                    <label for="username">Username:</label><br>
                    <input type="text" name="username" id="username" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label><br>
                    <input type="password" name="password" id="password" class="form-control">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary" type="button" id="btnLogin">Submit</button>
                  </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<script>
  document.getElementById("body").style.display = "block";
  var mp_url="<?php echo WEB_URL ?>";
  var base_url="<?php echo BASE_URL ?>";

  if(localStorage.token && localStorage.level){
    window.location.href = base_url;
  }

  if (typeof(Storage) === "undefined") {
    alert("Local storage not supported!")
  }

  $("#btnLogin").on("click",function(e){
    login()
  });

  $("input").on('keyup', function (e) {
    if (e.keyCode == 13) {
      login()
    }
  });

  function login(){
    var counter = 0
    $("input[type=text]").each(function() {
      if(this.value==""){
        $("input[name="+this.name+"]").addClass("is-invalid")
        counter++
      }else{
        $("input[name="+this.name+"]").removeClass("is-invalid")
      }
    });

    if(counter==0){
      $("#btnLogin").text("Verifying...")
      $("#btnLogin").prop("disabled", true)
      $.ajax({
        type: "POST",
        url: mp_url+"/auth/login",
        crossDomain: true,
        beforeSend: function(request) {
          request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          request.setRequestHeader("Accept", "application/json");
          request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
          request.setRequestHeader("Access-Control-Allow-Origin", "*");
        },
        data: $('#login-form').serialize(),
        dataType: "json",
        success: function(res){
          var authorizedLevel = ['w', 'a'];
          if(authorizedLevel.indexOf(res.level) === -1){
            alert("You have no access!")
            $("#btnLogin").text("Submit")
            $("#btnLogin").prop("disabled", false)
          }else{
            $("#btnLogin").text("Getting Data...")
            localStorage.token = "Bearer "+res.access_token
            localStorage.name = $("#username").val()
            localStorage.level = "admin"
            window.location.href = base_url;
          }
        },
        error: function (request, status, error) {
          var response = JSON.parse(request.responseText)
          console.log(response)
          alert(response.message)
          $("#btnLogin").text("Submit")
          $("#btnLogin").prop("disabled", false)
        }
      });
    }
  }
</script>
