<div class="row row-dashboard">
    <div class="col-md-12" style="margin-bottom:10px">
        <div class="row" style="margin-top:15px">
            <div class="col-md-12">
                <div class="card dash-card">
                    <div class="card-body">
                        <h5 class="card-title">Article Management</h5>
                        <hr>

                        <button type="button" class="btn btn-success" style="margin-bottom: 1em" onclick="onBtnArticleAddClick()"><i class="fas fa-plus"></i><b> New Article</b></button>
                        <span class="badge badge-secondary span-center" id="article-loading"><i class="fas fa-spinner ld ld-cycle"></i> Loading</span>
                        <table class="table table-hover" id="table-article" style="font-size:0.7rem; width: 100%;">
                            <thead class="thead-light">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Tag</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>

                        <!-- Modal Article -->
                        <div class="modal" id="modal-article">
                            <div class="modal-dialog" style="max-width: 75%">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Article Form</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <form action="">
                                            <div class="form-group">
                                                <label">Tag:</label>
                                                    <input class="form-control" id="article-tag" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Title:</label>
                                                <input class="form-control" id="article-title" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Image:</label>
                                                <input id="article-image" type="file" class="form-control-file border" accept="image/*" required>
                                                <p class="text-muted">Suggested size 650x400 or similar aspect ratio</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Content:</label>
                                                <div id="article-content" style="height: 250px"></div>
                                            </div>
                                            <input type="hidden" id="article-id">
                                            <input type="hidden" id="article-mode">
                                        </form>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <div class="col-md-12 text-center">
                                            <button id="article-button" type="button" class="btn btn-primary" onclick="onBtnArticleSaveClick()">Save Change</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Modal Article -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var articleHolder = '';
    var loadingState = true;

    // init datatables
    var table = $('#table-article').DataTable({
        responsive: true,
        "processing": true,
        "serverSide": false,
        columns: [{
                data: 'date_formated',
                width: '10%'
            },
            {
                data: 'tag',
                width: '10%'
            },
            {
                data: 'title',
                width: '30%'
            },
            {
                data: 'short_desc',
                width: '30%'
            },
            {
                data: 'action',
                width: '20%'
            }
        ],
        "order": [
            [0, "desc"]
        ]
    });

    // init quill editor
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        ['blockquote', 'code-block'],

        [{
            'header': 1
        }, {
            'header': 2
        }], // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'script': 'sub'
        }, {
            'script': 'super'
        }], // superscript/subscript
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent
        [{
            'direction': 'rtl'
        }], // text direction

        [{
            'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
            'header': [1, 2, 3, 4, 5, 6, false]
        }],
        ['link', 'image', 'video', 'formula'], // add's image support
        [{
            'color': []
        }, {
            'background': []
        }], // dropdown with defaults from theme
        [{
            'font': []
        }],
        [{
            'align': []
        }],

        ['clean'] // remove formatting button
    ];
    var quill = new Quill('#article-content', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });

    $(document).ready(function() {
        getContentArticle();
    });

    function loading(state) {
        if (state && !loadingState) {

            $("#article-button").empty();
            $("#article-button").html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $("#article-loading").show();
            loadingState = true;

        } else if (!state && loadingState) {

            $("#article-button").empty();
            $("#article-button").html('Save Change');
            $("#article-loading").hide();
            loadingState = false;

        }
    }

    function strip(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }

    function onBtnArticleAddClick() {
        $('#article-mode').val('add');
        $('#article-id').val('');
        $('#article-tag').val('');
        $('#article-title').val('');
        quill.setText('');
        $('#modal-article').modal('show');
    }

    function onBtnArticleSaveClick() {
        if ($('#article-mode').val() == 'add') {
            createContentArticle(
                $("#article-tag").val(),
                $("#article-title").val(),
                quill.container.firstChild.innerHTML,
                $("#article-image").prop('files')[0]
            );
        } else if ($('#article-mode').val() == 'edit') {
            var tempImage = '';
            if ($("#article-image").prop('files').length != 0) {
                tempImage = $("#article-image").prop('files')[0];
            }
            updateContentArticle(
                $("#article-tag").val(),
                $("#article-title").val(),
                quill.container.firstChild.innerHTML,
                tempImage,
                $("#article-id").val()
            );
        }
    }

    function onBtnArticleEditClick(element) {
        $('#article-mode').val('edit');
        var index = $(element).attr("data-index");
        $('#article-id').val(articleHolder[index].id);
        $('#article-tag').val(articleHolder[index].tag);
        $('#article-title').val(articleHolder[index].title);
        quill.clipboard.dangerouslyPasteHTML(articleHolder[index].content);
        $('#modal-article').modal('show');
    }

    function onBtnArticleDeleteClick(element) {
        if (confirm('Are you sure you want to delete this article ?')) {
            deleteContentArticle($(element).attr("data-id"));
        }
    }

    function getContentArticle() {
        loading(true);
        articleHolder = '';
        table.clear();
        $.ajax({
            type: "GET",
            url: mp_url + '/admin/content/article',
            crossDomain: true,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json");
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                request.setRequestHeader("Authorization", localStorage.token);
            },
            dataType: "json",
            success: function(res) {
                console.log(res);
                articleHolder = res;
                for (var x = 0; x < res.length; x++) {
                    var tempDate = new Date(res[x].created_at);
                    res[x].date_formated = tempDate.getDate() + '/' + tempDate.getMonth() + '/' + tempDate.getFullYear();
                    res[x].short_desc = strip(res[x].content).trim();
                    res[x].short_desc = res[x].short_desc.substring(0, 90) + '...';
                    res[x].action = '<button id="article-edit" class="btn btn-warning" data-index="' + x + '" onclick="onBtnArticleEditClick(this)"><i class="fas fa-edit" style="color: white"></i></button><button id="article-delete" style="margin-left: 0.5em" class="btn btn-danger" data-id="' + res[x].id + '" onclick="onBtnArticleDeleteClick(this)"><i class="fas fa-trash-alt"></i></button>';
                };
                table.rows.add(res).draw();
                loading(false);
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText)
                console.log(response);
                alert(response.message);
                loading(false);
            }
        });
    }

    function createContentArticle(tag, title, content, image) {
        loading(true);
        var fd = new FormData();
        fd.append("tag", tag);
        fd.append("title", title);
        fd.append("content", content);
        fd.append("image", image);
        console.log(fd);

        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/article',
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: fd,
            dataType: "json",
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentArticle();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentArticle();
            }
        });
    }

    function updateContentArticle(tag, title, content, image = '', id) {
        loading(true);
        var fd = new FormData();
        fd.append("tag", tag);
        fd.append("title", title);
        fd.append("content", content);
        fd.append("image", image);

        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/article/' + id,
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: fd,
            dataType: "json",
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentArticle();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentArticle();
            }
        });
    }

    function deleteContentArticle(articleId) {
        loading(true);
        $.ajax({
            type: "DELETE",
            url: mp_url + '/admin/content/article/' + articleId,
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentArticle();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentArticle();
            }
        });
    }
</script>