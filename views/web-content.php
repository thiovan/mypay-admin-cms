<div class="row row-dashboard">
    <div class="col-md-12" style="margin-bottom:10px">
        <div class="row" style="margin-top:15px">
            <div class="col-md-12">
                <div class="card dash-card">
                    <div class="card-body">
                        <h5 class="card-title">Website Content Management</h5>
                        <hr>

                        <span class="badge badge-secondary span-center" id="landing-loading"><i class="fas fa-spinner ld ld-cycle"></i> Loading</span>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="">
                                    <div class="form-group">
                                        <label>Website Title:</label>
                                        <input class="form-control" id="title">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Menu Navigation</h5>
                                    <div class="row" id="menu-container">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Button Navigation</h5>
                                    <div class="row" id="menu-button-container">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Home Section</h5>
                                    <div class="form-group">
                                        <label>Title:</label>
                                        <input class="form-control" id="home_title">
                                    </div>
                                    <div class="form-group">
                                        <label>Sub Title:</label>
                                        <textarea class="form-control" rows="2" id="home_sub_title"></textarea>
                                    </div>

                                    <br>
                                    <br>

                                    <h5>About Section</h5>
                                    <div class="form-group">
                                        <label>Title:</label>
                                        <input class="form-control" id="about_title">
                                    </div>
                                    <div class="form-group">
                                        <label>Description:</label>
                                        <textarea class="form-control" rows="3" id="about_desc"></textarea>
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Feature Section</h5>
                                    <div class="row" id="service-container">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Download Section</h5>
                                    <div class="form-group">
                                        <label>Title:</label>
                                        <input class="form-control" id="download_title">
                                    </div>
                                    <div class="form-group">
                                        <label>Description:</label>
                                        <textarea class="form-control" rows="3" id="download_desc"></textarea>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label>URL App Store (IOS):</label>
                                        <input class="form-control" id="download_ios">
                                    </div> -->
                                    <div class="form-group">
                                        <label>URL Play Store (Android):</label>
                                        <input class="form-control" id="download_android">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>FAQ Section</h5>
                                    <div class="row" id="faq-container">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Contact Section</h5>
                                    <div class="form-group">
                                        <label>Customer Service Contact:</label>
                                        <input class="form-control" id="cs_phone">
                                    </div>
                                    <div class="row" id="contact-container">
                                    </div>

                                    <br>
                                    <br>

                                    <h5>Image Assets</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Home Phone Image:</label>
                                                <input id="home_phone_image" type="file" class="form-control-file border" accept="image/x-png">
                                                <a id="home_phone_current" href="#" target="_blank"><i class="fas fa-link"></i> Current Image</a>
                                                <span>&nbsp;&nbsp;</span>
                                                <a id="home_phone_template" href="#" target="_blank"><i class="fas fa-link"></i> Template Image</a>
                                                <p class="text-muted">Make sure your image have same resolution as template image</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Feature Phone Image:</label>
                                                <input id="feature_phone_image" type="file" class="form-control-file border" accept="image/x-png">
                                                <a id="feature_phone_current" href="#" target="_blank"><i class="fas fa-link"></i> Current Image</a>
                                                <span>&nbsp;&nbsp;</span>
                                                <a id="feature_phone_template" href="#" target="_blank"><i class="fas fa-link"></i> Template Image</a>
                                                <p class="text-muted">Make sure your image have same resolution as template image</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Download Phone Image:</label>
                                                <input id="download_phone_image" type="file" class="form-control-file border" accept="image/x-png">
                                                <a id="download_phone_current" href="#" target="_blank"><i class="fas fa-link"></i> Current Image</a>
                                                <span>&nbsp;&nbsp;</span>
                                                <a id="download_phone_template" href="#" target="_blank"><i class="fas fa-link"></i> Template Image</a>
                                                <p class="text-muted">Make sure your image have same resolution as template image</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>About Logo Image:</label>
                                                <input id="about_logo_image" type="file" class="form-control-file border" accept="image/x-png">
                                                <a id="about_logo_current" href="#" target="_blank"><i class="fas fa-link"></i> Current Image</a>
                                                <span>&nbsp;&nbsp;</span>
                                                <a id="about_logo_template" href="#" target="_blank"><i class="fas fa-link"></i> Template Image</a>
                                                <p class="text-muted">Make sure your image have same resolution as template image</p>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>

                                    <button type="button" class="btn btn-primary btn-lg btn-block" id="submit" onclick="onBtnLandingSaveClick()">Save Change</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var contentHolder = '';
    var menuHolder = [];
    var menuButtonHolder = [];
    var serviceHolder = [];
    var faqHolder = [];
    var contactHolder = [];
    var serviceImageHolder = [];
    var loadingState = true;

    $(document).ready(function() {
        $("#home_phone_current").attr('href', mp_url_noapi + 'appy/images/header-mobile.png');
        $("#home_phone_template").attr('href', mp_url_noapi + 'appy/images/template/phone-image.png');
        $("#feature_phone_current").attr('href', mp_url_noapi + 'appy/images/feature-image.png');
        $("#feature_phone_template").attr('href', mp_url_noapi + 'appy/images/template/phone-image.png');
        $("#download_phone_current").attr('href', mp_url_noapi + 'appy/images/download-image.png');
        $("#download_phone_template").attr('href', mp_url_noapi + 'appy/images/template/phone-image.png');
        $("#about_logo_current").attr('href', mp_url_noapi + 'appy/images/about-logo.png');
        $("#about_logo_template").attr('href', mp_url_noapi + 'appy/images/template/default-logo.png');
        getContentLanding();
    });

    function loading(state) {
        if (state && !loadingState) {

            $("#landing-loading").show();
            $("#submit").html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $("#submit").attr("disabled", true);
            loadingState = true;

        } else if (!state && loadingState) {

            $("#landing-loading").hide();
            $("#submit").html('Save Change');
            $("#submit").removeAttr("disabled");
            loadingState = false;

        }
    }

    function renderMenuItem(items) {
        menuHolder = items;
        $("#menu-container").empty();
        items.forEach(function(item, index) {
            var menuItem = '<div class="col-md-3" style="padding: 0.75em"> <div class="form-group"> <label>Name:</label> <input class="form-control" id="menu_name_' + index + '" value="' + item.name + '"> </div> <div class="form-group"> <label>URL:</label> <input class="form-control" id="menu_href_' + index + '" value="' + item.href + '"> </div><button class="btn btn-outline-danger btn-block" type="button" data-id="' + index + '" onClick="onMenuItemDelete(this)"><i class="fas fa-trash-alt"></i>  Delete</button> </div><br>';
            $("#menu-container").append(menuItem);
        });

        var menuAdd = '<div class="border col-md-3 justify-content-center d-flex align-items-center" style="padding: 0.75em"><button class="btn btn-outline-success btn-lg" type="button" onClick="onMenuItemAdd(this)"><i class="fas fa-plus"></i><br>Add New Menu</button> </div><br>';
        $("#menu-container").append(menuAdd);
    }

    function onMenuItemDelete(element) {
        if (confirm('Are you sure you want to delete this menu ?')) {
            menuHolder.splice($(element).attr("data-id"), 1);
            $("#menu-container").empty();
            renderMenuItem(menuHolder);
        }
    }

    function onMenuItemAdd(element) {
        menuHolder.push({
            name: '',
            href: ''
        });
        $("#menu-container").empty();
        renderMenuItem(menuHolder);
    }

    function renderButtonMenuItem(items) {
        menuButtonHolder = items;
        $("#menu-button-container").empty();
        items.forEach(function(item, index) {
            var menuItem = '<div class="col-md-3" style="padding: 0.75em"> <div class="form-group"> <label>Name:</label> <input class="form-control" id="menu_button_name_' + index + '" value="' + item.name + '"> </div> <div class="form-group"> <label>URL:</label> <input class="form-control" id="menu_button_href_' + index + '" value="' + item.href + '"> </div><button type="button" class="btn btn-outline-danger btn-block" onClick="onMenuButtonItemDelete(this)" data-id="' + index + '"><i class="fas fa-trash-alt"></i>  Delete</button> </div><br>';
            $("#menu-button-container").append(menuItem);
        });

        var menuAdd = '<div class="border col-md-3 justify-content-center d-flex align-items-center" style="padding: 0.75em"><button type="button" class="btn btn-outline-success btn-lg" onClick="onMenuButtonItemAdd(this)"><i class="fas fa-plus"></i><br>Add New Button Menu</button> </div><br>';
        $("#menu-button-container").append(menuAdd);
    }

    function onMenuButtonItemDelete(element) {
        if (confirm('Are you sure you want to delete this button menu ?')) {
            menuButtonHolder.splice($(element).attr("data-id"), 1);
            $("#menu-button-container").empty();
            renderButtonMenuItem(menuButtonHolder);
        }
    }

    function onMenuButtonItemAdd(element) {
        menuButtonHolder.push({
            name: '',
            href: ''
        });
        $("#menu-button-container").empty();
        renderButtonMenuItem(menuButtonHolder);
    }

    function renderServiceItem(items) {
        serviceHolder = items;
        $("#service-container").empty();
        items.forEach(function(item, index) {
            var serviceItem = '<div class="col-md-4" style="padding: 0.75em"> <div class="row d-flex align-items-center"> <div class="col-md-4"> <img width="100px" src="' + mp_url_noapi + item.icon + '" /> </div> <div class="col-md-8"> <input id="service_icon_' + index + '" type="hidden" value="' + item.icon + '"> <input id="service_icon_image_"' + index + ' type="file" class="form-control-file border" accept="image/*" data-id="' + index + '" onChange="onServiceImageChange(this)"> </div> </div> <div class="form-group"> <label>Title:</label> <input class="form-control" id="service_title_' + index + '" value="' + item.title + '"> </div> <div class="form-group"> <label>Description:</label> <textarea class="form-control" rows="2" id="service_desc_' + index + '">' + item.desc + '</textarea> </div> <div class="form-group"> <label>URL:</label> <input class="form-control" id="service_href_' + index + '" value="' + item.href + '"> </div> <button type="button" class="btn btn-outline-danger btn-block" data-id="' + index + '" onClick="onServiceItemDelete(this)"><i class="fas fa-trash-alt"></i> Delete</button> </div>';
            $("#service-container").append(serviceItem);
        });
        var serviceAdd = '<div class="border col-md-4 justify-content-center d-flex align-items-center" style="padding: 0.75em"><button type="button" class="btn btn-outline-success btn-lg" onClick="onServiceItemAdd(this)"><i class="fas fa-plus"></i><br>Add New Feature</button> </div><br>';
        $("#service-container").append(serviceAdd);
    }

    function onServiceImageChange(element) {
        $("#service_icon_" + $(element).attr("data-id")).val('appy/images/' + $(element).prop('files')[0].name);
        serviceImageHolder.push($(element).prop('files')[0]);
    }

    function onServiceItemDelete(element) {
        if (confirm('Are you sure you want to delete this service ?')) {
            serviceHolder.splice($(element).attr("data-id"), 1);
            $("#service-container").empty();
            renderServiceItem(serviceHolder);
        }
    }

    function onServiceItemAdd(element) {
        serviceHolder.push({
            icon: '',
            title: '',
            desc: '',
            href: ''
        });
        $("#service-container").empty();
        renderServiceItem(serviceHolder);
    }

    function renderFaqItem(items) {
        faqHolder = items;
        $("#faq-container").empty();
        items.forEach(function(item, index) {
            var faqItem = '<div class="col-md-6" style="padding: 0.75em"> <div class="form-group"> <label>Question:</label> <input class="form-control" id="faq_question_' + index + '" value="' + item.question + '"> </div> <div class="form-group"> <label>Answer:</label> <textarea class="form-control" rows="3" id="faq_answer_' + index + '">' + item.answer + '</textarea> </div> <button type="button" class="btn btn-outline-danger btn-block" data-id="' + index + '" onClick="onFaqItemDelete(this)"><i class="fas fa-trash-alt"></i> Delete</button> </div>';
            $("#faq-container").append(faqItem);
        });
        var faqAdd = '<div class="border col-md-6 justify-content-center d-flex align-items-center" style="padding: 0.75em"><button type="button" class="btn btn-outline-success btn-lg" id="add-service" onClick="onFaqItemAdd(this)"><i class="fas fa-plus"></i><br>Add New FAQ</button> </div><br>';
        $("#faq-container").append(faqAdd);
    }

    function onFaqItemDelete(element) {
        if (confirm('Are you sure you want to delete this FAQ ?')) {
            faqHolder.splice($(element).attr("data-id"), 1);
            $("#faq-container").empty();
            renderFaqItem(faqHolder);
        }
    }

    function onFaqItemAdd(element) {
        faqHolder.push({
            question: '',
            answer: ''
        });
        $("#faq-container").empty();
        renderFaqItem(faqHolder);
    }

    function renderContactItem(items) {
        contactHolder = items;
        $("#contact-container").empty();
        items.forEach(function(item, index) {
            var contactItem = '<div class="col-md-4" style="padding: 0.75em"> <div class="form-group"> <label>Icon Name:</label> <input class="form-control" id="contact_icon_name_' + index + '" value="' + item.icon_name + '"> <a href="https://linearicons.com/free" target="_blank"><i class="fas fa-link"></i>  Icon Reference</a> </div> <div class="form-group"> <label>Description:</label> <input class="form-control" id="contact_line_1_' + index + '" value="' + item.line_1 + '"> <br> <input class="form-control" id="contact_line_2_' + index + '" value="' + item.line_2 + '"> </div> <button type="button" class="btn btn-outline-danger btn-block" data-id="' + index + '" onClick="onContactItemDelete(this)"><i class="fas fa-trash-alt"></i> Delete</button> </div>';
            $("#contact-container").append(contactItem);
        });
        var contactAdd = '<div class="border col-md-4 justify-content-center d-flex align-items-center" style="padding: 0.75em"><button type="button" class="btn btn-outline-success btn-lg" onClick="onContactItemAdd(this)"><i class="fas fa-plus"></i><br>Add New Contact</button> </div><br>';
        $("#contact-container").append(contactAdd);
    }

    function onContactItemDelete(element) {
        if (confirm('Are you sure you want to delete this Contact ?')) {
            contactHolder.splice($(element).attr("data-id"), 1);
            $("#contact-container").empty();
            renderContactItem(contactHolder);
        }
    }

    function onContactItemAdd(element) {
        contactHolder.push({
            icon_name: '',
            line_1: '',
            line_2: '',
        });
        $("#contact-container").empty();
        renderContactItem(contactHolder);
    }

    function getContentLanding() {
        loading(true);
        contentHolder = '';
        menuHolder = [];
        menuButtonHolder = [];
        serviceHolder = [];
        faqHolder = [];
        contactHolder = [];

        $.ajax({
            type: "GET",
            url: mp_url + '/admin/content/landing',
            crossDomain: true,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json");
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                request.setRequestHeader("Authorization", localStorage.token);
            },
            dataType: "json",
            success: function(res) {
                var content = JSON.parse(res);
                contentHolder = content;
                $("#title").val(content.title);
                renderMenuItem(content.menu);
                renderButtonMenuItem(content.menu_button);
                $("#home_title").val(content.home_title);
                $("#home_sub_title").val(content.home_sub_title);
                $("#about_title").val(content.about_title);
                $("#about_desc").val(content.about_desc);
                renderServiceItem(content.service);
                $("#download_title").val(content.download_title);
                $("#download_desc").val(content.download_desc);
                $("#download_ios").val(content.download_ios);
                $("#download_android").val(content.download_android);
                renderFaqItem(content.faq);
                $("#cs_phone").val(content.cs_phone);
                renderContactItem(content.contact);
                loading(false);
            }
        });
    }

    function onBtnLandingSaveClick() {
        var fd = new FormData()
        fd.append("title", JSON.stringify($("#title").val()));
        menuHolder.forEach(function(item, index) {
            item.name = $("#menu_name_" + index).val();
            item.href = $("#menu_href_" + index).val();
        });
        fd.append("menu", JSON.stringify(menuHolder));
        menuButtonHolder.forEach(function(item, index) {
            item.name = $("#menu_button_name_" + index).val();
            item.href = $("#menu_button_href_" + index).val();
        });
        fd.append("menu_button", JSON.stringify(menuButtonHolder));
        fd.append("home_title", JSON.stringify($("#home_title").val()));
        fd.append("home_sub_title", JSON.stringify($("#home_sub_title").val()));
        fd.append("about_title", JSON.stringify($("#about_title").val()));
        fd.append("about_desc", JSON.stringify($("#about_desc").val()));
        fd.append("download_title", JSON.stringify($("#download_title").val()));
        fd.append("download_desc", JSON.stringify($("#download_desc").val()));
        fd.append("download_ios", JSON.stringify($("#download_ios").val()));
        fd.append("download_android", JSON.stringify($("#download_android").val()));
        serviceHolder.forEach(function(item, index) {
            item.icon = $("#service_icon_" + index).val();
            item.title = $("#service_title_" + index).val();
            item.desc = $("#service_desc_" + index).val();
            item.href = $("#service_href_" + index).val();
        });
        fd.append("service", JSON.stringify(serviceHolder));
        faqHolder.forEach(function(item, index) {
            item.question = $("#faq_question_" + index).val();
            item.answer = $("#faq_answer_" + index).val();
        });
        fd.append("faq", JSON.stringify(faqHolder));
        contactHolder.forEach(function(item, index) {
            item.icon_name = $("#contact_icon_name_" + index).val();
            item.line_1 = $("#contact_line_1_" + index).val();
            item.line_2 = $("#contact_line_2_" + index).val();
        });
        fd.append("contact", JSON.stringify(contactHolder));
        fd.append("cs_phone", JSON.stringify($("#cs_phone").val()));
        fd.append("service_icon", serviceImageHolder);
        if ($("#home_phone_image").get(0).files.length !== 0) {
            fd.append("home_phone_image", $("#home_phone_image").prop('files')[0]);
        }
        if ($("#feature_phone_image").get(0).files.length !== 0) {
            fd.append("feature_phone_image", $("#feature_phone_image").prop('files')[0]);
        }
        if ($("#download_phone_image").get(0).files.length !== 0) {
            fd.append("download_phone_image", $("#download_phone_image").prop('files')[0]);
        }
        if ($("#about_logo_image").get(0).files.length !== 0) {
            fd.append("about_logo_image", $("#about_logo_image").prop('files')[0]);
        }
        updateContentLanding(fd);
    }

    function updateContentLanding(data) {
        loading(true);
        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/landing',
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: data,
            dataType: "json",
            success: function(res) {
                alert(res.message);
                getContentLanding();
                loading(false);
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentLanding();
                loading(false);
            }
        });
    }

    function debugFormData(fd) {
        for (var pair of fd.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
    }
</script>