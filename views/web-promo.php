<div class="row row-dashboard">
    <div class="col-md-12" style="margin-bottom:10px">
        <div class="row" style="margin-top:15px">
            <div class="col-md-12">
                <div class="card dash-card">
                    <div class="card-body">
                        <h5 class="card-title">Promo Management</h5>
                        <hr>

                        <button type="button" class="btn btn-success" style="margin-bottom: 1em" onclick="onBtnPromoAddClick()"><i class="fas fa-plus"></i><b> New Promo</b></button>
                        <span class="badge badge-secondary span-center" id="promo-loading"><i class="fas fa-spinner ld ld-cycle"></i> Loading</span>
                        <table class="table table-hover" id="table-promo" style="font-size:0.7rem; width: 100%;">
                            <thead class="thead-light">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Tag</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>

                        <!-- Modal Promo -->
                        <div class="modal" id="modal-promo">
                            <div class="modal-dialog" style="max-width: 75%">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <h4 class="modal-title">Promo Form</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <form action="">
                                            <div class="form-group">
                                                <label>Promo Title:</label>
                                                <input class="form-control" id="promo-title" required>
                                            </div>
                                            <div class="form-group">
                                                <label>Promo Image:</label>
                                                <input id="promo-image" type="file" class="form-control-file border" accept="image/*" required>
                                                <p class="text-muted">Suggested size 600x350 or similar aspect ratio</p>
                                            </div>
                                            <div class="form-group">
                                                <label>Promo Description:</label>
                                                <div id="promo-content" style="height: 250px"></div>
                                            </div>
                                            <input type="hidden" id="promo-tag" value="promo">
                                            <input type="hidden" id="promo-id">
                                            <input type="hidden" id="promo-mode">
                                        </form>
                                    </div>

                                    <!-- Modal footer -->
                                    <div class="modal-footer">
                                        <div class="col-md-12 text-center">
                                            <button id="promo-button" type="button" class="btn btn-primary" onclick="onBtnPromoSaveClick()">Save Change</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- Modal Promo -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var promoHolder = '';
    var loadingState = true;

    // init datatables
    var tablePromo = $('#table-promo').DataTable({
        responsive: true,
        "processing": true,
        "serverSide": false,
        columns: [{
                data: 'date_formated',
                width: '10%'
            },
            {
                data: 'tag',
                width: '10%'
            },
            {
                data: 'title',
                width: '30%'
            },
            {
                data: 'short_desc',
                width: '30%'
            },
            {
                data: 'action',
                width: '20%'
            }
        ],
        "order": [
            [0, "desc"]
        ]
    });

    // init quill editor
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        ['blockquote', 'code-block'],

        [{
            'header': 1
        }, {
            'header': 2
        }], // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'script': 'sub'
        }, {
            'script': 'super'
        }], // superscript/subscript
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent
        [{
            'direction': 'rtl'
        }], // text direction

        [{
            'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
            'header': [1, 2, 3, 4, 5, 6, false]
        }],
        ['link', 'image', 'video', 'formula'], // add's image support
        [{
            'color': []
        }, {
            'background': []
        }], // dropdown with defaults from theme
        [{
            'font': []
        }],
        [{
            'align': []
        }],

        ['clean'] // remove formatting button
    ];

    var quillPromo = new Quill('#promo-content', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });

    $(document).ready(function() {
        getContentPromo();
    });

    function strip(html) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }

    function loading(state) {
        if (state && !loadingState) {

            $("#promo-button").empty();
            $("#promo-button").html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $("#promo-loading").show();
            loadingState = true;

        } else if (!state && loadingState) {

            $("#promo-button").empty();
            $("#promo-button").html('Save Change');
            $("#promo-loading").hide();
            loadingState = false;

        }
    }

    function onBtnPromoAddClick() {
        $('#promo-mode').val('add');
        $('#promo-id').val('');
        $('#promo-tag').val('promo');
        $('#promo-title').val('');
        quillPromo.setText('');
        $('#modal-promo').modal('show');
    }

    function onBtnPromoSaveClick() {
        if ($('#promo-mode').val() == 'add') {
            console.log($("#promo-tag").val());
            console.log($("#promo-title").val());
            console.log(quillPromo.container.firstChild.innerHTML);
            console.log($("#promo-image").prop('files')[0]);
            createContentArticle(
                $("#promo-tag").val(),
                $("#promo-title").val(),
                quillPromo.container.firstChild.innerHTML,
                $("#promo-image").prop('files')[0]
            );
        } else if ($('#promo-mode').val() == 'edit') {
            var tempImage = '';
            if ($("#promo-image").prop('files').length != 0) {
                tempImage = $("#promo-image").prop('files')[0];
            }
            updateContentArticle(
                $("#promo-tag").val(),
                $("#promo-title").val(),
                quillPromo.container.firstChild.innerHTML,
                tempImage,
                $("#promo-id").val()
            );
        }
    }

    function onBtnPromoEditClick(element) {
        $('#promo-mode').val('edit');
        var index = $(element).attr("data-index");
        $('#promo-id').val(promoHolder[index].id);
        $('#promo-tag').val(promoHolder[index].tag);
        $('#promo-title').val(promoHolder[index].title);
        quillPromo.clipboard.dangerouslyPasteHTML(promoHolder[index].content);
        $('#modal-promo').modal('show');
    }

    function onBtnPromoDeleteClick(element) {
        if (confirm('Are you sure you want to delete this promo ?')) {
            deleteContentArticle($(element).attr("data-id"));
        }
    }

    function getContentPromo() {
        loading(true);
        promoHolder = '';
        tablePromo.clear();
        $.ajax({
            type: "GET",
            url: mp_url + '/admin/content/promo',
            crossDomain: true,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json");
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                request.setRequestHeader("Authorization", localStorage.token);
            },
            dataType: "json",
            success: function(res) {
                promoHolder = res;
                for (var x = 0; x < res.length; x++) {
                    var tempDate = new Date(res[x].created_at);
                    res[x].date_formated = tempDate.getDate() + '/' + tempDate.getMonth() + '/' + tempDate.getFullYear();
                    res[x].short_desc = strip(res[x].content).trim();
                    res[x].short_desc = res[x].short_desc.substring(0, 90) + '...';
                    res[x].action = '<button id="promo-edit" class="btn btn-warning" data-index="' + x + '" onclick="onBtnPromoEditClick(this)"><i class="fas fa-edit" style="color: white"></i></button><button id="promo-delete" style="margin-left: 0.5em" class="btn btn-danger" data-id="' + res[x].id + '" onclick="onBtnPromoDeleteClick(this)"><i class="fas fa-trash-alt"></i></button>';
                }
                tablePromo.rows.add(res).draw();
                loading(false);

            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText)
                console.log(response)
                alert(response.message)
                loading(false);

            }
        });
    }

    function createContentArticle(tag, title, content, image) {
        loading(true);
        var fd = new FormData();
        fd.append("tag", tag);
        fd.append("title", title);
        fd.append("content", content);
        fd.append("image", image);
        console.log(fd);

        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/article',
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: fd,
            dataType: "json",
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentPromo();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentPromo();
            }
        });
    }

    function updateContentArticle(tag, title, content, image = '', id) {
        loading(true);
        var fd = new FormData();
        fd.append("tag", tag);
        fd.append("title", title);
        fd.append("content", content);
        fd.append("image", image);

        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/article/' + id,
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: fd,
            dataType: "json",
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentPromo();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentPromo();
            }
        });
    }

    function deleteContentArticle(articleId) {
        loading(true);
        $.ajax({
            type: "DELETE",
            url: mp_url + '/admin/content/article/' + articleId,
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function(request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            success: function(res) {
                console.log(res);
                alert(res.message);
                getContentPromo();
            },
            error: function(request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentPromo();
            }
        });
    }
</script>