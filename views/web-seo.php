<div class="row row-dashboard">
    <div class="col-md-12" style="margin-bottom:10px">
        <div class="row" style="margin-top:15px">
            <div class="col-md-12">
                <div class="card dash-card">
                    <div class="card-body">
                        <h5 class="card-title">Search Engine Optimization</h5>
                        <hr>

                        <span class="badge badge-secondary span-center" id="seo-loading"><i class="fas fa-spinner ld ld-cycle"></i> Loading</span>
                        <div class="row">
                            <div class="col-md-12">
                                <form action="">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Meta Title:</label>
                                                <input class="form-control" id="meta_title" maxlength="70">
                                                <p class="text-muted">Title must be within 70 Characters</p>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Meta Description:</label>
                                                <input class="form-control" id="meta_description" maxlength="150">
                                                <p class="text-muted">Description must be within 150 Characters</p>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Meta Keywords:</label>
                                                <input class="form-control" id="meta_keywords">
                                                <p class="text-muted">Example: keywords1, keywords2, keywords3</p>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Meta Author:</label>
                                                <input class="form-control" id="meta_author">
                                                <p class="text-muted">Website owner or organization name</p>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Allow robots to index your website ?</label>
                                                <select class="form-control" id="meta_robot_index">
                                                    <option value="index">Yes</option>
                                                    <option value="noindex">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Allow robots to follow all links ?</label>
                                                <select class="form-control" id="meta_robot_follow">
                                                    <option value="follow">Yes</option>
                                                    <option value="nofollow">No</option>
                                                </select>
                                            </div>
                                        </div>

                                        <br>

                                    </div>

                                    <button type="button" class="btn btn-primary btn-lg btn-block" id="submit" onclick="onBtnLandingSaveClick()">Save Change</button>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var loadingState = true;
    
    $(document).ready(function() {
        getContentSeo();
    });

    function loading(state) {
        if (state && !loadingState) {

            $("#seo-loading").show();
            $("#submit").html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...`
            );
            $("#submit").attr("disabled", true);
            loadingState = true;

        } else if (!state && loadingState) {

            $("#seo-loading").hide();
            $("#submit").html('Save Change');
            $("#submit").removeAttr("disabled");
            loadingState = false;

        }
    }

    function getContentSeo() {
        loading(true);

        $.ajax({
            type: "GET",
            url: mp_url + '/admin/content/landing',
            crossDomain: true,
            beforeSend: function (request) {
                request.setRequestHeader("Accept", "application/json");
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                request.setRequestHeader("Authorization", localStorage.token);
            },
            dataType: "json",
            success: function (res) {
                var content = JSON.parse(res);
                contentHolder = content;
                $("#meta_title").val(content.meta_title);
                $("#meta_description").val(content.meta_description);
                $("#meta_keywords").val(content.meta_keywords);
                $("#meta_author").val(content.meta_author);
                var tempRobots = String(content.meta_robots).split(", ");
                $("#meta_robot_index").val(tempRobots[0]);
                $("#meta_robot_follow").val(tempRobots[1]);
                loading(false);
            }
        });
    }

    function onBtnLandingSaveClick() {
        var fd = new FormData()
        fd.append("meta_title", JSON.stringify($("#meta_title").val()));
        fd.append("meta_description", JSON.stringify($("#meta_description").val()));
        fd.append("meta_keywords", JSON.stringify($("#meta_keywords").val()));
        fd.append("meta_author", JSON.stringify($("#meta_author").val()));
        fd.append("meta_robots", JSON.stringify($("#meta_robot_index").val() + ', ' + $("#meta_robot_follow").val()));
        updateContentLanding(fd);
    }

    function updateContentLanding(data) {
        loading(true);
        $.ajax({
            type: "POST",
            url: mp_url + '/admin/content/landing',
            crossDomain: true,
            processData: false,
            contentType: false,
            beforeSend: function (request) {
                request.setRequestHeader("Accept", "application/json")
                request.setRequestHeader("X-Requested-With", "XMLHttpRequest")
                request.setRequestHeader("Authorization", localStorage.token)
            },
            data: data,
            dataType: "json",
            success: function (res) {
                alert(res.message);
                getContentSeo();
                loading(false);
            },
            error: function (request, status, error) {
                var response = JSON.parse(request.responseText);
                console.log(response);
                alert(response.message);
                getContentSeo();
                loading(false);
            }
        });
    }

    function debugFormData(fd) {
        for (var pair of fd.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
    }
</script>